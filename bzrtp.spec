Name:           bzrtp
Version:        4.5.15
Release:        7%{?dist}
Summary:        ZRTP keys exchange protocol

License:        GPLv3+
URL:            https://github.com/BelledonneCommunications/%{name}
Source0:        https://github.com/BelledonneCommunications/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
# FIXME: cleanup and send upstream
Patch0:         %{name}-pkgconfig.patch

BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  bctoolbox-devel
BuildRequires:  libxml2-devel
BuildRequires:  sqlite-devel

%description
BZRTP is an opensource implementation of ZRTP keys exchange protocol. The
library written in C 89 is fully portable and can be executed on many
platforms including both ARM processor and x86.

%package        devel
Summary:        Header files and libraries for bzrtp development
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       libxml2-devel%{?_isa}
Requires:       sqlite-devel%{?_isa}

%description    devel
The %{name}-devel package contains the header files
and libraries for use with bzrtp package.


%prep
%autosetup -p1


%build
%cmake -DENABLE_DOC=ON -DENABLE_STATIC=OFF
%cmake_build


%install
%cmake_install
rm -rf %{buildroot}%{_docdir}/%{name}*/


%ldconfig_scriptlets


%files
%{_datadir}/%{name}/
%{_libdir}/libbzrtp.so.0
%license LICENSE.txt
%doc CHANGELOG.md
%doc README.md

%files devel
%doc %{_vpath_builddir}/doc/html/
%{_includedir}/%{name}/
%{_libdir}/libbzrtp.so
%{_libdir}/pkgconfig/libbzrtp.pc


%changelog
* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-7
- Build development documentation

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-6
- libxml2 dependency for devel package

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-5
- Change license to GPLv3+

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-4
- Fix sqlite dependency

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-3
- sqlite dependency for devel package

* Mon May 24 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-2
- Pkgconfig file patch

* Sun May 23 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.15-1
- Initial version
